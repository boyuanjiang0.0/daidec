require('dotenv').config()
const mysql = require("mysql");


console.log(process.env.USER)
var db = mysql.createPool({
  host: process.env.HOST,
  user: process.env.USER,
  password: process.env.PASSWORD,
    // database: process.DATABASE,
  multipleStatements: true,
});

db.getConnection(function (err) {
  if (err) {
    console.log("DB connect error ...");
    console.log(err);
    throw err;
  }
});

db.services = function (callback) {
  db.query("SELECT * from API", function (err, rows) {
    callback(err, rows);
  });
};

db.saveService = function (data, callback) {
  db.query("insert into API set ?", [data], function (err, rows) {
    console.log("Err: ", err);
    console.log("The solution is: ", rows);
    callback(err, rows);
  });
};

module.exports = db;
