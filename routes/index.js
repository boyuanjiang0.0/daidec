var express = require('express');
var router = express.Router();
var db = require("../dbService");




/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post("/api/name_exact", async (req, res) => {
  console.log("print the body",req.body)
  const result = await db.query(
    `SELECT * FROM daidec.cars where Name = '${req.body.name}';`,
    (err, result) => {
      if (err) {
        return res.status(200).json({
          success: false,
          data: err,
        });
      } else {
        return res.status(200).json({
          success: true,
          data: result,
        });
      }
    }
  );
  return result;
});


router.post("/api/name_contain", async (req, res) => {
  console.log("print the body",req.body)
  const result = await db.query(
    `SELECT * FROM daidec.cars where Name LIKE  '%${req.body.name}%';`,
    (err, result) => {
      if (err) {
        return res.status(200).json({
          success: false,
          data: err,
        });
      } else {
        return res.status(200).json({
          success: true,
          data: result,
        });
      }
    }
  );
  return result;
});

module.exports = router;
